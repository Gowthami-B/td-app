import Vue from 'vue'
import Router from 'vue-router'
import Landing from './views/landing.vue'
import SignUp from './views/signUp'
import SignIn from './views/signIn'
import Home from './views/home'
import User from './views/user'
import UserDetails from './views/user-details'


Vue.use(Router)

export default new Router({
    routes: [

        {
            path: '/',
            name: 'landing',
            component: Landing
        },
        {
            path: '/signup',
            name: 'signUp',
            component: SignUp
        },
        {
            path: '/signin',
            name: 'signIn',
            component: SignIn
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },
        {
            path: '/user/:id',
            name: 'user',
            component: User,
            props: true,
            children: [
                {
                    path: '/user-details',
                    name: 'user-details',
                    component: UserDetails
                },
            ],
        },

    ]
})
